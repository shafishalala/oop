public class Student {
    private String name;
    private int age;
    private int grade;

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public int getGrade() {
        return grade;
    }

    public void displayInfo() {
        System.out.println("Name: " + name);
        System.out.println("Age: " + age);
        System.out.println("Grade: " + grade);
    }

    // Example usage
    public static void main(String[] args) {
        Student student = new Student();
        student.setName("Shalala");
        student.setAge(25);
        student.setGrade(89);

        student.displayInfo();
    }
}